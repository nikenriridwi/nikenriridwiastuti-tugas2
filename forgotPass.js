import React, {useState} from 'react';
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';

const ForgotPass = ({navigation}) => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <View style={{marginTop: 45, marginRight: 317}}>
            <Image source={require('./assets/back-button.png')}></Image>
          </View>
        </TouchableOpacity>

        <Image
          source={require('./assets/marlin.png')}
          style={{marginTop: 140}}></Image>
        <Text style={styles.text}>Reset your password</Text>
        <TextInput
          style={styles.input}
          keyboardType="email-address"
          placeholder="Email"></TextInput>

        <TouchableOpacity
          style={styles.button}
          onPress={() => Alert.alert('Request sent!')}>
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              fontSize: 12,
              fontStyle: 'normal',
            }}>
            Request Reset
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    alignItems: 'center',
  },

  text: {
    marginTop: 17,
    color: '#5B5B5B',
    fontSize: 18,
    fontStyle: 'normal',
    fontFamily: 'Nunito',
  },

  input: {
    width: 273,
    height: 43,
    backgroundColor: 'white',
    marginTop: 33,
    borderRadius: 4,
  },

  button: {
    justifyContent: 'center',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    borderRadius: 4,
    marginTop: 17,
  },
});

export default ForgotPass;
