import React from 'react';
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MyTabBar from './BottomTab';

const Tab = createBottomTabNavigator();

function HomeScreen() {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.container2}>
          <View style={styles.container3}>
            <Image
              source={require('./assets/banner.png')}
              style={styles.image}></Image>
          </View>

          <View style={styles.image2}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Image source={require('./assets/ic_ferry_intl.png')}></Image>
            </View>

            <View style={{flex: 1, alignItems: 'center'}}>
              <Image source={require('./assets/ic_ferry_domestic.png')}></Image>
            </View>

            <View style={{flex: 1, alignItems: 'center'}}>
              <Image source={require('./assets/ic_attraction.png')}></Image>
            </View>

            <View style={{flex: 1, alignItems: 'center'}}>
              <Image source={require('./assets/ic_pioneership.png')}></Image>
            </View>
          </View>

          <TouchableOpacity style={styles.button}>
            <Text style={{color: 'white', textAlign: 'center', fontSize: 16}}>
              More...
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

function MyBooking() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>MyBooking!</Text>
    </View>
  );
}

function Profile() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Profile!</Text>
    </View>
  );
}

function Help() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Help!!</Text>
    </View>
  );
}

function MyTab() {
  return (
    <Tab.Navigator
      screenOptions={{headerShown: false}}
      tabBar={props => <MyTabBar {...props} />}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="My Booking" component={MyBooking} />
      <Tab.Screen name="Profile" component={Profile} />
      <Tab.Screen name="Help" component={Help} />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {},
  container2: {
    width: '100%',
    backgroundColor: '#F4F4F4',
  },

  container3: {
    width: '100%',
    flexDirection: 'row',
    backgroundColor: '#F4F4F4',
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    aspectRatio: 2.5,
  },

  image2: {
    flexDirection: 'row',
    marginTop: 22,
    justifyContent: 'center',
  },

  button: {
    justifyContent: 'center',
    backgroundColor: '#2E3283',
    marginHorizontal: 126,
    height: 43,
    borderRadius: 10,
    marginTop: 350,
    marginBottom: 13,
  },
});

export default MyTab;
