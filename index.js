/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
// import Signup from './Signup';
// import Login from './Login';
// import forgotPass from './forgotPass';
// import Home from './Home';

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
