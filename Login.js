import React, {useState} from 'react';
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';

import TextBox from 'react-native-password-eye';

const Login = ({navigation}) => {
  const [password, onChangePassword] = React.useState('');
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.container2}>
          <Image
            source={require('./assets/marlin.png')}
            style={{marginTop: 140}}></Image>
          <Text style={styles.text}>Please sign in to continue</Text>
          <TextInput
            style={styles.input}
            keyboardType="default"
            placeholder="Username"></TextInput>

          <TextBox
            onChangeText={onChangePassword}
            keyboardType="password"
            secureTextEntry={true}
            placeholder="************"
            containerStyles={styles.input2}
            eyeColor="2E2E2E"></TextBox>

          <TouchableOpacity
            onPress={() => navigation.navigate('Home')}
            style={styles.button}>
            <Text
              style={{
                color: 'white',
                textAlign: 'center',
                fontSize: 12,
                fontStyle: 'normal',
              }}>
              Sign In
            </Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate('ForgotPass')}>
            <Text
              style={{
                color: '#2E3283',
                fontSize: 11,
                fontStyle: 'normal',
                fontFamily: 'Nunito',
                marginTop: 17,
              }}>
              Forgot Password
            </Text>
          </TouchableOpacity>

          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 33}}>
            <View
              style={{
                flex: 1,
                height: 1,
                backgroundColor: 'black',
                marginLeft: 60,
              }}
            />
            <View>
              <Text
                style={{
                  width: 100,
                  textAlign: 'center',
                  color: '#2E3283',
                  fontSize: 11,
                  fontStyle: 'normal',
                }}>
                Login With
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                height: 1,
                backgroundColor: 'black',
                marginRight: 60,
              }}
            />
          </View>

          <View style={{flexDirection: 'row', marginTop: 36}}>
            <View style={{marginLeft: 50}}>
              <TouchableOpacity onPress={() => Alert.alert('Open Facebook')}>
                <Image
                  source={require('./assets/fb.png')}
                  style={{width: 46, height: 46, marginRight: 44}}></Image>
              </TouchableOpacity>
              <Text
                style={{
                  color: '#828282',
                  marginTop: 33,
                  fontSize: 12,
                  fontStyle: 'normal',
                }}>
                App version
              </Text>
            </View>
            <View>
              <TouchableOpacity onPress={() => Alert.alert('Open Google')}>
                <Image
                  source={require('./assets/google.png')}
                  style={{width: 46, height: 46, marginRight: 44}}></Image>
              </TouchableOpacity>
              <Text
                style={{
                  color: '#828282',
                  marginTop: 33,
                  fontSize: 12,
                  fontStyle: 'normal',
                }}>
                2.8.3
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>

      <View style={styles.footer}>
        <Text style={{textAlign: 'center'}}>
          <Text>
            <Text>Don't have account?</Text>
            <Text
              onPress={() => navigation.navigate('Signup')}
              style={{fontWeight: 'bold'}}>
              Sign up
            </Text>
          </Text>
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    alignItems: 'center',
  },

  container2: {
    backgroundColor: '#F4F4F4',
    alignItems: 'center',
  },

  text: {
    marginTop: 17,
    color: '#5B5B5B',
    fontSize: 18,
    fontStyle: 'normal',
    fontFamily: 'Nunito',
  },

  input: {
    width: 273,
    height: 43,
    backgroundColor: 'white',
    marginTop: 33,
    borderRadius: 4,
  },

  input2: {
    width: 273,
    height: 43,
    backgroundColor: 'white',
    marginTop: 17,
    borderRadius: 4,
  },

  button: {
    justifyContent: 'center',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    borderRadius: 4,
    marginTop: 17,
  },

  footer: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
    height: 35,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});

export default Login;
