import React from 'react';
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';

const Signup = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.container2}>
          <Image
            source={require('./assets/marlin.png')}
            style={{marginTop: 140, justifyContent: 'center'}}></Image>
          <Text style={styles.text}>Create an account</Text>
          <TextInput
            style={styles.input}
            keyboardType="default"
            placeholder="Name"></TextInput>

          <TextInput
            style={styles.input2}
            keyboardType="email-address"
            placeholder="Email"></TextInput>

          <TextInput
            style={styles.input3}
            keyboardType="numeric"
            placeholder="Phone"></TextInput>

          <TextInput
            style={styles.input4}
            keyboardType="password"
            secureTextEntry
            placeholder="Password"></TextInput>

          <TouchableOpacity
            onPress={() => navigation.navigate('Login')}
            style={styles.button}>
            <Text
              style={{
                color: 'white',
                textAlign: 'center',
                fontSize: 12,
                fontStyle: 'normal',
              }}>
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>

      <View style={styles.footer}>
        <Text style={{textAlign: 'center'}}>
          <Text style={{}}>
            <Text style={{}}>Already have account?</Text>
            <Text
              onPress={() => navigation.navigate('Login')}
              style={{fontWeight: 'bold'}}>
              Log In
            </Text>
          </Text>
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    alignItems: 'center',
  },

  container2: {
    backgroundColor: '#F4F4F4',
    alignItems: 'center',
  },

  text: {
    marginTop: 17,
    color: '#5B5B5B',
    fontSize: 18,
    fontStyle: 'normal',
    fontFamily: 'Nunito',
  },

  input: {
    width: 273,
    height: 43,
    backgroundColor: 'white',
    marginTop: 33,
    borderRadius: 4,
  },

  input2: {
    width: 273,
    height: 43,
    backgroundColor: 'white',
    marginTop: 17,
    borderRadius: 4,
  },

  input3: {
    width: 273,
    height: 43,
    backgroundColor: 'white',
    marginTop: 17,
    borderRadius: 4,
  },

  input4: {
    width: 273,
    height: 43,
    backgroundColor: 'white',
    marginTop: 17,
    borderRadius: 4,
  },

  button: {
    justifyContent: 'center',
    backgroundColor: '#2E3283',
    width: 273,
    height: 43,
    borderRadius: 4,
    marginTop: 17,
    marginBottom: 62,
  },

  footer: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
    height: 35,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});

export default Signup;
